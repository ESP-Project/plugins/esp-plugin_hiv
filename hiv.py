'''
                                  ESP Health
                         Notifiable Diseases Framework
                           HIV Case Generator


@author: Chaim Kirby <ckirby@commoninf.com>
@organization: commonwealth informatics http://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2015 
@license: LGPL
'''

from collections import OrderedDict
from decimal import Decimal

from ESP.conf.models import LabTestMap
from ESP.hef.base import Event
from ESP.hef.base import LabResultPositiveHeuristic, LabResultAnyHeuristic, LabResultFixedThresholdHeuristic
from ESP.hef.base import PrescriptionHeuristic, DiagnosisHeuristic, Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.utils import log
from django.db import IntegrityError
from django.db.models import Count, Max

from multidrugwindow import MultiDrugWindowAnalysis

HIV_CRITERIA = {
    'a': "Criteria 1a: Positive Western Blot, Multispot, Geenius, or AB Diff",
    'b': "Criteria 1b: Positive HIV Antigen/Antibody and Positive ELISA",
    'c': "Criteria 1c: HIV Viral Load greater than 200",
    'd': "Criteria 1d: HIV Qualitative PCR",
    'e': 'Criteria 1e. >=2 ICD codes for HIV and history of prescription for >=3 HIV meds ever',
    # 'f': '1f',
    'g': "Criteria 1g. 3 or more hiv concurrent prescriptions for 3 months"
}


class PotentialHIVCase(object):
    def __init__(self, date, criteria, event, events):
        self.date = date
        self.criteria = criteria
        self.event = event
        self.events = events


class PotentialHIVCaseList(object):
    def __init__(self, patient):
        self.patient = patient
        self.cases = []

    def _get_affirming_date(self):
        names = ['lx:hiv_ag_ab:positive', 'lx:hiv_elisa:positive']
        return self._get_event_date(names)

    def _get_negating_date(self):
        names = ['lx:hiv_ag_ab:negative', 'lx:hiv_elisa:negative']
        return self._get_event_date(names)

    def _get_event_date(self, names):
        return Event.objects.filter(patient=self.patient, name__in=names).aggregate(Max('date'))['date__max']

    def add_case(self, case):
        self.cases.append(case)
        log.debug("{} has {} cases".format(self.patient, len(self.cases)))

    def get_earliest_good_case(self):
        self.cases.sort(key=lambda phc: phc.date)
        negative = self._get_negating_date()
        positive = self._get_affirming_date()
        log.debug("Neg Date {}".format(negative))
        log.debug("Pos Date {}".format(positive))
        for case in self.cases:
            log.debug("Case Date {}".format(case.date))
            log.debug("Case Criteria {}".format(case.criteria))
            if case.criteria in [HIV_CRITERIA['a'], HIV_CRITERIA['b'], HIV_CRITERIA['c'], HIV_CRITERIA['d']]:
                log.debug("Good Criteria, Good Case")
                return case
            else:
                log.debug("In Date Test")
                if negative is None or negative < case.date:
                    log.debug("No or less Neg, Good Case")
                    return case
                elif positive is not None and positive > case.date:
                    log.debug("Has Pos, Good Case")
                    return case
        log.debug("No Good Case")
        return None

    def gather_events(self):
        ev = []
        for case in self.cases:
            ev.extend(case.events)
        return set(ev)


class HIV(DiseaseDefinition):
    '''
    HIV
    '''

    conditions = ['hiv']

    uri = 'urn:x-esphealth:disease:channing:hiv:v1'

    short_name = 'hiv'

    test_name_search_strings = [
        'hiv',
        'immuno',
        'cd4'
    ]

    timespan_heuristics = []

    # recurrence_interval = 360 #TODO episode length  1 year

    def __init__(self, *args, **kwargs):
        super(HIV, self).__init__(*args, **kwargs)
        self.heuristics = {}
        self._lab_heuristics(self.heuristics.setdefault("labs", []))
        self._diagnosis_heuristics(self.heuristics.setdefault("diagnosis", []))
        self._prescription_heuristics(self.heuristics.setdefault("prescriptions", OrderedDict()))
        self.potential_cases = {}

    def _lab_heuristics(self, lab_list):
        for lab_name in ['hiv_elisa', 'hiv_ag_ab', 'hiv_pcr', 'hiv_wb',
                         'hiv_rna_viral', 'hiv_multispot', 'hiv_geenius', 'hiv_ab_diff']:
            lab_list.append(LabResultPositiveHeuristic(
                test_name=lab_name,
            ))

        lab_list.append(
            LabResultFixedThresholdHeuristic(
                test_name='hiv_rna_viral',
                match_type='gt',
                threshold=Decimal(200),
            )
        )

        lab_list.append(LabResultAnyHeuristic(test_name="cd4"))

    def _diagnosis_heuristics(self, diagnosis_list):
        diagnosis_list.append(DiagnosisHeuristic(
            name='hiv',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='042', type='icd9'),
                Dx_CodeQuery(starts_with='V08', type='icd9'),
                Dx_CodeQuery(starts_with='B20', type='icd10'),
                Dx_CodeQuery(starts_with='B21', type='icd10'),
                Dx_CodeQuery(starts_with='B22', type='icd10'),
                Dx_CodeQuery(starts_with='B23', type='icd10'),
                Dx_CodeQuery(starts_with='B24', type='icd10'),
                Dx_CodeQuery(starts_with='Z21', type='icd10'),
                Dx_CodeQuery(starts_with='B97.35', type='icd10'),
                Dx_CodeQuery(starts_with='O98.7', type='icd10'),
            ]
        ))

    def _prescription_heuristics(self, prescription_dict):
        '''
        The form of the name is used to parse the names number of drugs out of a prescription heuristic.
        This is done in multidrugwindow and is used to create a distinct set of drug names prescribed within a window
        Current expectation is drugname1-drugname2-...-drugnameN.
        Optionally, the suffix ":generic" may be added.
        '''
        prescriptions = {
            'single': {
                'zidovudine': {'drugs': ['Zidovudine ', 'Retrovir'],
                               'exclude': ['Trizivir', 'Combivir', 'Abacav', 'Lamivu'],
                               'require': []},
                'didanosine': {'drugs': ['Didanosine', 'Videx'], 'exclude': [], 'require': []},
                'stavudine': {'drugs': ['Stavudine', 'Zerit'], 'exclude': [], 'require': []},
                'lamivudine': {'drugs': ['Lamivudine', 'Epivir'],
                               'exclude': ['Epzicom', 'Trizivir', 'Triumeq', 'Abacav', 'Zidov', 'Dolut', 'Efavirenz', 'Tenof', 
                                           'Symfi', 'Dolato', 'Efavi', 'Hbv', 'Dorav', 'Cimduo', 'Delstrigo', 'Temixys'],
                               'require': []},
                'emtricitabine': {'drugs': ['Emtricitabine', 'Emtriva'],
                                  'exclude': ['Truvada', 'Atripla', 'Complera', 'Stribild', 'Tenof', 'Efavi', 'Genvoya',
                                              'Rilpiv', 'Elvit', 'Cob', 'Descovy', 'Odefsey', 'Biktarvy', 'Bicte', 'Symtuza'], 'require': []},
                'tenofovir': {'drugs': ['Tenofovir', 'Viread'],
                              'exclude': ['Truvada', 'Atripla', 'Complera', 'Stribild', 'Emtric', 'Efavi', 'Genvoya',
                                          'Rilpiv', 'Elvit', 'Cob', 'Descovy', 'Odefsey', 'Biktarvy', 'Bicte', 'Symfi', 
                                          'Symtuza', 'Lamivu', 'Dorav', 'Cimduo', 'Delstrigo', 'Temixys'],
                              'require': []},
                'abacavir': {'drugs': ['Abacavir', 'Ziagen'],
                             'exclude': ['Epzicom', 'Trizivir', 'Triumeq', 'Lamivu', 'Zidov', 'Dolut'],
                             'require': []},
                'efavirenz': {'drugs': ['Efavirenz', 'Sustiva'], 'exclude': ['Atripla', 'Emitric', 'Tenof', 'Lamivu', 'Symfi'],
                              'require': []},
                'nivarapine': {'drugs': ['Nivarapine', 'Viramune'], 'exclude': [], 'require': []},
                'rilpivirine': {'drugs': ['Rilpivirine', 'Edurant'],
                                'exclude': ['Complera', 'Tenof', 'Emtric', 'Odefsey', 'Juluca', 'Dolut', 'Cabot', 'Cabenuva'], 'require': []},
                'etravirine': {'drugs': ['Etravirine', 'Intelence'], 'exclude': [], 'require': []},
                'delavirdine': {'drugs': ['Delavirdine', 'Rescriptor'], 'exclude': [], 'require': []},
                'raltegravir': {'drugs': ['Raltegravir', 'Isentress'], 'exclude': [], 'require': []},
                'dolutegravir': {'drugs': ['Dolutegravir', 'Tivicay'],
                                 'exclude': ['Triumeq', 'Abacav', 'Lamivu', 'Dovato', 'Juluca', 'Riton', 'Rilpiv'],
                                 'require': []},
                'elvitegravir': {'drugs': ['Elvitegravir', 'Vitekta' ],
                                 'exclude': ['Stribild', 'Cob', 'Tenof', 'Emtric', 'Genvoya'], 'require': []},
                'enfuvirtide': {'drugs': ['Enfuvirtide', 'Fuzeon'], 'exclude': [], 'require': []},
                'maraviroc': {'drugs': ['Maraviroc', 'Selzentry'], 'exclude': [], 'require': []},
                'tipranavir': {'drugs': ['Tipranavir', 'Aptivus'], 'exclude': [], 'require': []},
                'ritonavir': {'drugs': ['Ritonavir', 'Norvir'], 'exclude': ['Kaletra', 'Lopina', 'Ombit', 'Parit', 'Nirmat'], 'require': []},
                'indinavir': {'drugs': ['Indinavir', 'Crixivan'], 'exclude': [], 'require': []},
                'darunavir': {'drugs': ['Darunavir', 'Prezista'], 'exclude': ['Prezcobix', 'Cob', 'Symtuza', 'Tenof', 'Emtri'], 'require': []},
                'saquinavir': {'drugs': ['Saquinavir', 'Invirase'], 'exclude': [], 'require': []},
                'atazanavir': {'drugs': ['Atazanavir', 'Reyataz'], 'exclude': ['Evotaz', 'Cob'], 'require': []},
                'nelfinavir': {'drugs': ['Nelfinavir', 'Viracept'], 'exclude': [], 'require': []},
                'fosamprenavir': {'drugs': ['Fosamprenavir', 'Lexiva'], 'exclude': [], 'require': []},
                'doravirine': {'drugs': ['Doravirine', 'Pifeltro'], 'exclude': ['Tenof', 'Emtri', 'Lamivu', 'Delstrigo'], 'require': []},
                'cabotegravir': {'drugs': ['Cabotegravir', 'Vocabria'], 'exclude': ['Rilpiv', 'Cabenuva', 'Apretude', 'Cabotegravir ER 600'], 'require': []},
                'cabotegravir_er_600': {'drugs': ['Cabotegravir ER 600' , 'Apretude'], 'exclude': ['Rilpiv', 'Cabenuva', 'Vocabria'], 'require': []},
                'lenacapavir': {'drugs': ['Lenacapavir', 'Sunlenca'], 'exclude': [], 'require': []},
                'fostemsavir': {'drugs': ['Fostemsavir', 'Rukobia'], 'exclude': [], 'require': []},
                'ibalizumab_uiyk': {'drugs': ['Ibalizumab-Uiyk', 'Trogarzo'], 'exclude': [], 'require': []},

            },
            'double': {
                'tenofovir-emtricitabine': {'drugs': ['Truvada'],
                                            'exclude': ['Emtriva', 'Viread', 'Complera', 'Odefsey'],
                                            'require': []},
                'tenofovir-emtricitabine:generic': {'drugs': ['Tenof'],
                                                    'exclude': ['Truvada', 'Emtriva', 'Viread', 'Complera', 'Efavi',
                                                                'Rilpiv', 'Elvit', 'Cob', 'Descovy', 'Odefsey', 'Biktarvy', 'Bicte', 'Darun',
                                                                'Tenofovir Alafen', 'Tenofovir AF', 'Tenofov Alafen'],
                                                    'require': ['Emtric']},
                                                    
                'tenofovir_alafenamide-emtricitabine': {'drugs': ['Descovy'],
                                            'exclude': ['Emtriva', 'Viread', 'Complera', 'Odefsey', 'Truvada'],
                                            'require': []},
                'tenofovir_alafenamide-emtricitabine:generic': {'drugs': ['Tenofovir Alafen', 'Tenofovir AF', 'Tenofov Alafen'],
                                                    'exclude': ['Truvada', 'Emtriva', 'Viread', 'Complera', 'Efavi',
                                                                'Rilpiv', 'Elvit', 'Cob', 'Descovy', 'Odefsey', 'Biktarvy', 'Bicte', 'Darun'],
                                                    'require': ['Emtric']},                                                   
                         
                         
                                                    
                'zidovudine-lamivudine': {'drugs': ['Combivir', ], 'exclude': ['Retrovir'],
                                          'require': []},
                'zidovudine-lamivudine:generic': {'drugs': ['Zidov'], 'exclude': ['Retrovir', 'Combivir', 'Abacav'],
                                                  'require': ['Lamivu']},
                'atazanavir-cobicistat': {'drugs': ['Evotaz', ], 'exclude': ['Retrovir'],
                                          'require': []},
                'atazanavir-cobicistat:generic': {'drugs': ['Atazanavir'], 'exclude': ['Retrovir', 'Combivir', 'Abacav', 'Evotaz'],
                                                  'require': ['Cob']},
                'dolutegravir-lamivudine': {'drugs': ['Dovato', ], 'exclude': ['Retrovir'],
                                          'require': []},
                'dolutegravir-lamivudine:generic': {'drugs': ['Dolut'], 'exclude': ['Retrovir', 'Combivir', 'Abacav', 'Dovato'],
                                                  'require': ['Lamivu']},       
                'dolutegravir-rilpivirine': {'drugs': ['Juluca', ], 'exclude': ['Retrovir'],
                                          'require': []},
                'dolutegravir-rilpivirine:generic': {'drugs': ['Dolut'], 'exclude': ['Retrovir', 'Combivir', 'Abacav', 'Dovato', 'Juluca'],
                                                  'require': ['Rilpiv']},                                                  
                'abacavir-lamivudine': {'drugs': ['Epzicom'],
                                        'exclude': ['Epivir', 'Ziagen'],
                                        'require': []},
                'abacavir-lamivudine:generic': {'drugs': ['Abacav'],
                                                'exclude': ['Epivir', 'Ziagen', 'Epzicom', 'Zidov', 'Dolut'],
                                                'require': ['Lamivu']},
                'lopinavir-ritonavir': {'drugs': ['Kaletra'], 'exclude': ['Norvir'], 'require': []},
                'lopinavir-ritonavir:generic': {'drugs': ['Lopina'], 'exclude': ['Kaletra', 'Norvir'],
                                                'require': ['Riton']},
                'darunavir-cobicistat': {'drugs': ['Prezcobix'], 'exclude': ['Prezista'], 'require': []},
                'darunavir-cobicistat:generic': {'drugs': ['Darun'], 'exclude': ['Prezista', 'Prezcobix', 'Emtri', 'Symtuza'],
                                                 'require': ['Cob']},
                'lamivudine-tenofovir': {'drugs': ['Cimduo', 'Temixys'], 'exclude': [], 'require': []},
                'lamivudine-tenofovir:generic': {'drugs': ['Lamivu'], 'exclude': ['Cimduo', 'Temixys', 'Symfi', 'Efavi', 'Dorav'],
                                                 'require': ['Tenof']},
                'cabotegravir-rilpivirine': {'drugs': ['Cabenuva'], 'exclude': [], 'require': []},
                'cabotegravir-rilpivirine:generic': {'drugs': ['Cabotegr'], 'exclude': ['Cabenuva'],
                                                 'require': ['Rilpiv']},                              
                
            },
            'triple': {
                'abacavir-lamivudine-zidovudine': {'drugs': ['Trizivir'],
                                                   'exclude': ['Epzicom', 'Combivir', 'Retrovir',
                                                               'Epivir', 'Ziagen'],
                                                   'require': []},
                'abacavir-lamivudine-zidovudine:generic': {'drugs': ['Abacav'],
                                                           'exclude': ['Epzicom', 'Combivir', 'Retrovir',
                                                                       'Epivir', 'Ziagen', 'Trizivir'],
                                                           'require': ['Lamivu', 'Zidov']},
                'efavirenz-lamivudine-tenofovir': {'drugs': ['Symfi'],
                                                      'exclude': ['Emtriva', 'Truvada',
                                                                  'Viread'], 'require': []},
                'efavirenz-lamivudine-tenofovir:generic': {'drugs': ['Efavi'],
                                                              'exclude': ['Emtriva', 'Truvada',
                                                                          'Viread', 'Atripla', 'Symfi', 'Emtric'],
                                                              'require': ['Tenof', 'Lamivu']},
                'efavirenz-tenofovir-emtricitabine': {'drugs': ['Atripla'],
                                                      'exclude': ['Emtriva', 'Truvada',
                                                                  'Viread'], 'require': []},
                'efavirenz-tenofovir-emtricitabine:generic': {'drugs': ['Efavi'],
                                                              'exclude': ['Emtriva', 'Truvada',
                                                                          'Viread', 'Atripla'],
                                                              'require': ['Tenof', 'Emtric']},
                'rilpivirine-tenofovir-emtricitabine': {'drugs': ['Complera', 'Odefsey'],
                                                        'exclude': ['Emtriva', 'Truvada',
                                                                    'Viread', 'Edurant', 'Descovy'],
                                                        'require': []},
                'rilpivirine-tenofovir-emtricitabine:generic': {'drugs': ['Rilpiv'],
                                                                'exclude': ['Emtriva', 'Truvada',
                                                                            'Viread', 'Edurant', 'Complera', 'Descovy', 'Odefsey'],
                                                                'require': ['Tenof', 'Emtric']},
                'dolutegravir-abacavir-lamivudine': {'drugs': ['Triumeq'],
                                                     'exclude': ['Epzicom', 'Epivir',
                                                                 'Ziagen', 'Epzicom', 'Tivicay'],
                                                     'require': []},
                'dolutegravir-abacavir-lamivudine:generic': {'drugs': ['Dolut'],
                                                             'exclude': ['Epzicom', 'Epivir',
                                                                         'Ziagen', 'Epzicom', 'Tivicay', 'Triumeq'],
                                                             'require': ['Abacav', 'Lamivu']},
                'bictegravir-emtricitabine-tenofovir_alafenamide': {'drugs': ['Biktarvy'],
                                                                        'exclude': ['Emtriva', 'Viread'],
                                                                        'require': []},
                'bictegravir-emtricitabine-tenofovir_alafenamide:generic': {'drugs': ['Bicte'],
                                                                        'exclude': ['Emtriva', 'Viread', 'Descovy', 'Truvada',
                                                                                    'Complera', 'Efavi', 'Rilpiv', 'Elvit', 'Cob', 'Odefsey', 'Biktarvy'],
                                                                        'require': ['Emtric', 'Tenof']},
                'doravirine-lamivudine-tenofovir': {'drugs': ['Delstrigo'], 'exclude': [], 'require': []},
                'doravirine-lamivudine-tenofovir:generic': {'drugs': ['Dorav'], 'exclude': ['Delstrigo'],
                                                 'require': ['Lamivu', 'Tenof']},
            },
            'quad': {
                'darunavir-cobicistat-tenofovir-emtricitabine': {'drugs': ['Symtuza'],
                                                                    'exclude': ['Emtriva', 'Truvada', 'Atripla',
                                                                                'Complera', 'Viread'],
                                                                    'require': []},
                'darunavir-cobicistat-tenofovir-emtricitabine:generic': {'drugs': ['Darun'],
                                                                            'exclude': ['Emtriva', 'Truvada', 'Atripla',
                                                                                        'Complera', 'Viread',
                                                                                        'Stribild', 'Genvoya', 'Symtuza'],
                                                                            'require': ['Cob', 'Tenof', 'Emtri']},
                'elvitegravir-cobicistat-tenofovir-emtricitabine': {'drugs': ['Stribild', 'Genvoya'],
                                                                    'exclude': ['Emtriva', 'Truvada', 'Atripla',
                                                                                'Complera', 'Viread', 'Vitekta'],
                                                                    'require': []},
                'elvitegravir-cobicistat-tenofovir-emtricitabine:generic': {'drugs': ['Elvit'],
                                                                            'exclude': ['Emtriva', 'Truvada', 'Atripla',
                                                                                        'Complera', 'Viread',
                                                                                        'Stribild', 'Genvoya', 'Vitekta'],
                                                                            'require': ['Cob', 'Tenof', 'Emtric']},
            }
        }

        prescription_dict.setdefault("four_drug", [PrescriptionHeuristic(name='hiv_%s' % key,
                                                                         drugs=val['drugs'],
                                                                         exclude=val['exclude'],
                                                                         require=val['require'])
                                                   for key, val in prescriptions['quad'].items()])
        prescription_dict.setdefault("three_drug", [PrescriptionHeuristic(name='hiv_%s' % key,
                                                                          drugs=val['drugs'],
                                                                          exclude=val['exclude'],
                                                                          require=val['require'])
                                                    for key, val in prescriptions['triple'].items()])
        prescription_dict.setdefault("two_drug", [PrescriptionHeuristic(name='hiv_%s' % key,
                                                                        drugs=val['drugs'],
                                                                        exclude=val['exclude'],
                                                                        require=val['require'])
                                                  for key, val in prescriptions['double'].items()])
        prescription_dict.setdefault("one_drug", [PrescriptionHeuristic(name='hiv_%s' % key,
                                                                        drugs=val['drugs'],
                                                                        exclude=val['exclude'],
                                                                        require=val['require'])
                                                  for key, val in prescriptions['single'].items()])

    def _extract_lists_from_dict_tree(self, container, holder=None):
        if holder is None:
            holder = []
        if not isinstance(container, dict):
            raise TypeError("container must be of type dict, got %s" % type(container))
        for key, val in container.items():
            if isinstance(val, list):
                holder.extend(val)
            else:
                self._extract_lists_from_dict_tree(val, holder)

        return holder

    @property
    def event_heuristics(self):
        return self._extract_lists_from_dict_tree(self.heuristics)

    def _generate_cases_by_criteria(self, criteria_events, criteria):
        self.criteria = criteria
        criteria_events = criteria_events.exclude(case__condition=self.conditions[0]).order_by('date').values('pk',
                                                                                                              'name',
                                                                                                              'patient',
                                                                                                              'date')

        criteria_patients = set()
        criteria_patient_events = {}
        for event in criteria_events:
            ept = event['patient']
            criteria_patients.add(ept)
            try:
                criteria_patient_events[ept].append(event)
            except KeyError:
                criteria_patient_events[ept] = [event]

        patients_with_existing_cases = self.add_events_to_existing_cases(criteria_patients,
                                                                         [self._extract_pks(criteria_patient_events)])
        patids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        for patient in (criteria_patients - patids_for_existing_cases):
            event = criteria_patient_events[patient][0]
            phc = PotentialHIVCase(criteria_patient_events[patient][0]['date'],
                                   self.criteria,
                                   event['pk'],
                                   self._extract_pks(criteria_patient_events)[patient])

            self.populate_potential_cases(patient, phc)

    def _generate_prescription_cases(self):
        rxs = []
        prescription_heur = self.heuristics.get("prescriptions", {})
        presc = self._extract_lists_from_dict_tree(prescription_heur)
        for x in presc:
            rxs.append(x.rx_event_name)

        rx_events = Event.objects.filter(
            name__in=rxs,
        ).exclude(case__condition=self.conditions[0]).order_by('date').values('pk', 'name', 'patient', 'date').exclude(name='rx:hiv_cabotegravir_er_600')

        rx_patient_events = {}
        for event in rx_events:
            try:
                rx_patient_events[event['patient']].append(event)
            except KeyError:
                rx_patient_events[event['patient']] = [event]

        patients_with_existing_cases = self.add_events_to_existing_cases(rx_patient_events.keys(),
                                                                         [self._extract_pks(rx_patient_events)])
        patids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        if len(rx_patient_events.keys()) > 0:
            self.criteria = HIV_CRITERIA['g']

        for patient, events in rx_patient_events.items():
            if patient in patids_for_existing_cases:
                continue

            potential_drug_cases = []

            for event in events:
                for pdc in potential_drug_cases:
                    pdc.add_prescription(event)
                multi_drugs = MultiDrugWindowAnalysis()
                multi_drugs.add_prescription(event)
                potential_drug_cases.append(multi_drugs)

            for multi_drug_window in potential_drug_cases:
                satisfying_events = multi_drug_window.satisfies_criteria()
                if satisfying_events is not None:
                    phc = PotentialHIVCase(satisfying_events[0]['date'],
                                           self.criteria,
                                           satisfying_events[0]['pk'],
                                           [e['pk'] for e in events])

                    self.populate_potential_cases(patient, phc)

    def _generate_dx_prescription_cases(self):
        rxs = []
        prescription_heur = self.heuristics.get("prescriptions", {})
        presc = self._extract_lists_from_dict_tree(prescription_heur)
        for x in presc:
            rxs.append(x.rx_event_name)

        rx_events = Event.objects.filter(
            name__in=rxs,
        ).exclude(case__condition=self.conditions[0]).order_by('date').values('pk', 'name', 'patient', 'date')

        dx_events = Event.objects.filter(
            name__in=['dx:hiv'],
        ).exclude(case__condition=self.conditions[0]).order_by('date').values('pk', 'name', 'patient', 'date')

        rx_patient_events = {}
        for event in rx_events:
            try:
                rx_patient_events[event['patient']].append(event)
            except KeyError:
                rx_patient_events[event['patient']] = [event]

        dx_patient_events = {}
        for event in dx_events:
            try:
                dx_patient_events[event['patient']].append(event)
            except KeyError:
                dx_patient_events[event['patient']] = [event]

        patients_with_existing_cases = self.add_events_to_existing_cases(
            list(set(rx_patient_events.keys()) | set(dx_patient_events.keys())),
            [self._extract_pks(rx_patient_events),
             self._extract_pks(dx_patient_events)])
        patids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        if len(rx_patient_events.keys()) > 0:
            self.criteria = HIV_CRITERIA['e']

        for patient, events in dx_patient_events.items():
            if patient in patids_for_existing_cases:
                continue

            all_dx_events = events
            try:
                patient_rx_events = rx_patient_events[patient]
            except KeyError:
                continue

            prx_count = sum(len(pre['name'].split('-')) for pre in patient_rx_events)
            if prx_count > 2:
                window_dx_events = all_dx_events
                while len(window_dx_events) > 1:
                    phc = PotentialHIVCase(window_dx_events[0]['date'],
                                           self.criteria,
                                           window_dx_events[0]['pk'],
                                           [e['pk'] for e in events] + self._extract_pks(rx_patient_events)[patient]
                                           )

                    self.populate_potential_cases(patient, phc)

                    window_dx_events = window_dx_events[1:]

    def populate_potential_cases(self, patient, phc):
        self.potential_cases[patient] = self.potential_cases.get(patient, PotentialHIVCaseList(patient))
        log.debug('Should add additional Case {}'.format(patient))
        self.potential_cases[patient].add_case(phc)

    def _process_potential_cases(self):
        new_cases = 0
        log.debug('Patients with potential cases %s' % len(self.potential_cases))
        for patient, potential_list in self.potential_cases.items():
            log.debug('Processing cases for %s' % patient)
            t = False
            patient_cases = potential_list
            first_case = patient_cases.get_earliest_good_case()
            if first_case is None:
                log.debug('No good cases for %s' % patient)
                continue
            try:
                log.debug('Good Case for %s' % patient)
                t, new_case = self._create_case_from_event_list(
                    condition=self.conditions[0],
                    criteria=first_case.criteria,
                    recurrence_interval=None,  # Does not recur
                    event_obj=Event.objects.get(pk=first_case.event),
                    relevant_event_names=set(patient_cases.gather_events()) - set([first_case.event]),
                )
            except IntegrityError:
                log.debug('Case creation failed for %s' % patient)
                pass  # _create_case_from_event_list only knows how to create a case and return True.
            if t:
                new_cases += 1
                log.info('Created new hiv case %s : %s' % (first_case.criteria, new_case))

        return new_cases

    def _extract_pks(self, patient_event_dict):
        return {patient: [event['pk'] for event in events] for patient, events in patient_event_dict.items()}

    def generate_hiv(self):
        self._generate_cases_by_criteria(Event.objects.filter(
            name__in=['lx:hiv_wb:positive', 'lx:hiv_multispot:positive', 'lx:hiv_geenius:positive', 'lx:hiv_ab_diff:positive']),
            HIV_CRITERIA['a']
        )

        ag_e_names = ['lx:hiv_ag_ab:positive', 'lx:hiv_elisa:positive']
        self._generate_cases_by_criteria(
            Event.objects.filter(name__in=ag_e_names, patient__event__name__in=ag_e_names)
                .distinct()
                .annotate(ev_names=Count('patient__event__name', distinct=True))
                .filter(ev_names=len(ag_e_names)),
            HIV_CRITERIA['b']
        )

        self._generate_cases_by_criteria(
            Event.objects.filter(name__in=['lx:hiv_rna_viral:threshold:gt:200', 'lx:hiv_rna_viral:positive']),
            HIV_CRITERIA['c']
        )

        self._generate_cases_by_criteria(Event.objects.filter(name__in=['lx:hiv_pcr:positive']), HIV_CRITERIA['d'])

        self._generate_prescription_cases()
        self._generate_dx_prescription_cases()

        return self._process_potential_cases()

    def generate(self):
        LabTestMap.create_update_dummy_lab(self.conditions[0], 'MDPH-293', 'MDPH-R467')
        log.info('Generating cases of %s' % self.short_name)
        counter = self.generate_hiv()
        log.info('Generated %s new cases of HIV' % counter)

        return counter  # Count of new cases

    def report_field(self, report_field, case):
        reportable_fields = {
            'na_trmt_obx': False,
            'symptom_obx': False,
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
            'last_dx_na': None if case.encounters.count() == 0 else 'NA-1510',
            'last_dx_date': None if case.encounters.count() == 0  else self._get_dx_date(case),

        }

        return reportable_fields.get(report_field, None)

    def _get_dx_date(self, case):
        return case.encounters.last().date


# -------------------------------------------------------------------------------
#
# Packaging
#
# -------------------------------------------------------------------------------

hiv_definition = HIV()


def event_heuristics():
    return hiv_definition.event_heuristics


def disease_definitions():
    return [hiv_definition]
