'''
Created on Mar 30, 2016

@author: bobz
'''
import pdb
from datetime import timedelta

WINDOWSIZE = 90
SCNDWINDOWSTRTMIN = 30
SCNDWINDOWSTRTMAX = 400


class MedWindowStatus(object):
    Insufficient, Out_Of_Range, In_Range = range(3)  # may need another status here "filled"


class MedWindow(object):
    def __init__(self, prescription):
        self.start_date = prescription['date']
        self.end_date = prescription['date'] + timedelta(WINDOWSIZE)
        self.names = set()
        for name in prescription['name'].replace(':generic', '').replace('tenofovir_alafenamide', 'tenofovir').split('-'):
            self.names.add(name)
        self.prescriptions = []
        self.drug_count = 0
        self.last_prescription_date = prescription['date']
        self.populate_silos(prescription)

    def add_prescription(self, prescription):
        if prescription['date'] > self.end_date:
            return MedWindowStatus.Insufficient if not self.has_sufficient_drugs() else MedWindowStatus.Out_Of_Range
        if self.has_sufficient_drugs() and prescription['date'] > self.last_prescription_date:
            return MedWindowStatus.In_Range
        self.last_prescription_date = prescription['date']
        populated_len = len(self.names)
        for name in prescription['name'].replace(':generic', '').replace('tenofovir_alafenamide', 'tenofovir').split('-'):
            self.names.add(name)
        if len(self.names) > populated_len:
            self.populate_silos(prescription)
        return MedWindowStatus.In_Range

    def has_sufficient_drugs(self):
        return self.drug_count > 2

    # think about additional method/property that identifies sufficient windows and stops adding prescriptions

    def populate_silos(self, prescription):
        self.prescriptions.append(prescription)
        self.drug_count = len(self.names)


class MultiDrugWindowAnalysis(object):
    def __init__(self):
        self.windows = []
        self.insufficient = []

    def add_prescription(self, prescription):
        for window in reversed(self.windows):
            status = window.add_prescription(prescription)
            if status == MedWindowStatus.Out_Of_Range:
                break
            if status == MedWindowStatus.Insufficient:
                self.insufficient.append(window)
                break
        self.windows.append(MedWindow(prescription))

    def satisfies_criteria(self):
        sufficient_windows = [window for window in self.windows if window not in self.insufficient]
        if len(sufficient_windows) < 2:
            return None
        else:
            sufficient_windows.sort(key=lambda window: window.start_date)
        for idx, window in enumerate(sufficient_windows, 1):
            while idx < len(sufficient_windows):
                if (window.start_date + timedelta(SCNDWINDOWSTRTMIN)
                        <= sufficient_windows[idx].start_date
                        <= window.start_date + timedelta(SCNDWINDOWSTRTMAX)
                    and window.last_prescription_date < sufficient_windows[idx].start_date
                    and self.windows_satisfy(window, sufficient_windows[idx])):
                    return window.prescriptions
                elif sufficient_windows[idx].start_date > window.start_date + timedelta(SCNDWINDOWSTRTMAX):
                    break  # second window is out of range -- no need to keep checking
                else:
                    idx = idx + 1
                if idx > len(sufficient_windows) - 1:  # End of the line (idx iteration starts at 1, actual list index starts at 0).
                    break
        return None

    def windows_satisfy(self, first, second):
        if len(first.names) >= 3 and len(second.names) >= 3:
            return True
        return False
