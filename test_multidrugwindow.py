import unittest, pdb
from datetime import date, timedelta, datetime
from multidrugwindow import MultiDrugWindowAnalysis


class MockEvent(object):
    def __init__(self, date, id, name):
        self.date = date
        self.name = name
        self.id = id

    def __getitem__(self, item):
        return self.__dict__[item]

class TestMultiDrugWindow(unittest.TestCase):
    def setUp(self):
        self.mdwa = MultiDrugWindowAnalysis()
        self.now = date.today()

    def tearDown(self):
        self.mdwa = None
        self.now = None

    def test_add_prescription_till_satisfied(self):
        self.mdwa.add_prescription(MockEvent(self.now - timedelta(180),'E1', 'drug1-drug3'))
        self.assertEqual(len(self.mdwa.windows), 1) #t1: Should have 1 window
        self.mdwa.add_prescription(MockEvent(self.now - timedelta(180),'E2', 'drug2'))
        self.assertEqual(len(self.mdwa.windows), 2) #t2: Should have 2 windows
        self.assertEqual(self.mdwa.windows[0].drug_count, 3) #t4: First window should have 3 drugs
        self.assertTrue(self.mdwa.windows[0].has_sufficient_drugs()) #t5: first window should have sufficient drugs
        self.assertEqual(self.mdwa.windows[1].drug_count, 1) #t7: second window should have 1 drug
        self.assertFalse(self.mdwa.windows[1].has_sufficient_drugs()) #t8: Second window should NOT have sufficient drugs
        self.assertIsNone(self.mdwa.satisfies_criteria()) #t9: windows do not meet criteria
        self.mdwa.add_prescription(MockEvent(self.now - timedelta(90),'E3', 'drug1-drug2'))
        self.assertEqual(len(self.mdwa.windows), 3) #t10: Should have 3 windows
        self.assertEqual(self.mdwa.windows[0].drug_count, 3) #t12: First window should have 3 drugs
        self.assertEqual(self.mdwa.windows[1].drug_count, 2) #t14: Second window should have 2 drugs
        self.assertEqual(self.mdwa.windows[2].drug_count, 2) #t15: third window should have 2 drugs
        self.assertIsNone(self.mdwa.satisfies_criteria()) #t16: windows do not meet criteria
        self.mdwa.add_prescription(MockEvent(self.now - timedelta(5),'E4', 'drug4'))
        self.assertEqual(len(self.mdwa.windows), 4) #t17: Should have 4 windows
        self.assertEqual(self.mdwa.windows[0].drug_count, 3) #t18: First window should have 3 drugs
        self.assertEqual(self.mdwa.windows[2].drug_count, 3) #t19: third window should have 3 drugs
        self.assertIsNotNone(self.mdwa.satisfies_criteria()) #t20: windows meet criteria
        
    def test_long_sequence(self):
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2007-02-21','%Y-%m-%d'),'27416371','efavirenz-tenofovir-emtricitabine'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2007-10-31','%Y-%m-%d'),'27456875','tenofovir-emtricitabine'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2007-10-31','%Y-%m-%d'),'27429378','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2007-10-31','%Y-%m-%d'),'27546486','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2008-11-14','%Y-%m-%d'),'27430104','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2008-11-14','%Y-%m-%d'),'27457823','tenofovir-emtricitabine'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2008-11-14','%Y-%m-%d'),'27546732','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2008-12-29','%Y-%m-%d'),'27546752','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2009-12-14','%Y-%m-%d'),'27430904','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2009-12-14','%Y-%m-%d'),'27547142','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2009-12-14','%Y-%m-%d'),'27430905','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2009-12-14','%Y-%m-%d'),'27458888','tenofovir-emtricitabine'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2009-12-14','%Y-%m-%d'),'27458887','tenofovir-emtricitabine'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2009-12-14','%Y-%m-%d'),'27547143','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-01-07','%Y-%m-%d'),'27550057','tenofovir-emtricitabine:generic'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-01-07','%Y-%m-%d'),'27547723','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-01-07','%Y-%m-%d'),'27431975','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-11-07','%Y-%m-%d'),'27432662','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-11-07','%Y-%m-%d'),'27550464','tenofovir-emtricitabine:generic'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-11-07','%Y-%m-%d'),'27548086','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2012-11-29','%Y-%m-%d'),'27433485','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2012-11-29','%Y-%m-%d'),'27551028','tenofovir-emtricitabine:generic'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2012-11-29','%Y-%m-%d'),'27548573','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-02-01','%Y-%m-%d'),'27551161','tenofovir-emtricitabine:generic'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-02-01','%Y-%m-%d'),'27548673','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-02-01','%Y-%m-%d'),'27433645','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-03-15','%Y-%m-%d'),'27433726','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-03-25','%Y-%m-%d'),'27551234','tenofovir-emtricitabine:generic'))
        self.assertIsNotNone(self.mdwa.satisfies_criteria()) #t1: windows does meet criteria
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-03-25','%Y-%m-%d'),'27433749','ritonavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-03-25','%Y-%m-%d'),'27548749','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-04-25','%Y-%m-%d'),'27551291','tenofovir-emtricitabine:generic'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-04-25','%Y-%m-%d'),'27548823','darunavir'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2013-04-25','%Y-%m-%d'),'27433833','ritonavir'))

    def test_generic_suffix_removal(self):
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-11-07','%Y-%m-%d'),'1','tenofovir-emtricitabine:generic'))
        self.mdwa.add_prescription(MockEvent(datetime.strptime('2011-11-08','%Y-%m-%d'),'2','tenofovir-emtricitabine'))
        self.assertEqual(self.mdwa.windows[0].drug_count, 2) #t1: SHould be two drugs even though event name is different (generic doesn't count as different)
        
    
if __name__=='__main__':
    unittest.main()
